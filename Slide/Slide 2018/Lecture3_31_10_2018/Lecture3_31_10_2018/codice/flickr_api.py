
# -*- coding: utf-8 -*-
#!/usr/bin/env python
#title:        flickr_api.py
#description:    Download a list of public images from Flickr with associated information. Good images are placed in /images, not found images are placed in /missing.
#author:    Alessandro Ortis
#last update:        20181030
#version:    0.1
#usage:        python flickr_api.py
#
#notes:        Installare flickrapi tramite il comando
#            pip install flickrapi
#
#        Installare ftfy, scikit-image, opencv
#            pip install ftfy==4.4.3
#            pip install scikit-image
#            pip install opencv-python
#
#        Creare le cartelle 'images' e 'missing', inoltre creare la cartella 'data' con i template per le immagini mancanti
#
#            Seguire le istruzioni dettagliate sul sito web per configurare il token di accesso a Flickr
#            https://stuvel.eu/flickrapi-doc/
#
#==============================================================================


import flickrapi
import json
import pickle  # serve solo se si salvano dati binari
import urllib, urllib2
import csv
import sys, os, os.path
reload(sys)
sys.setdefaultencoding('utf8')
import time
from skimage.measure import compare_ssim as ssim
import cv2
import numpy as np
from sys import argv
from shutil import copyfile
import time, datetime, calendar
import ftfy



# questa funzione verifica l'esistenza di una foto scaricata 
def isMissing(imagePath):
    for referenceImage in nullImages:
        refImgPath = "data/" + referenceImage
        refImg = cv2.imread(refImgPath)
        curImg = cv2.imread(imagePath)
        
            
        refImg = cv2.cvtColor(refImg, cv2.COLOR_BGR2GRAY)
        curImg = cv2.cvtColor(curImg, cv2.COLOR_BGR2GRAY)
        
        curImg = cv2.resize(curImg,(refImg.shape[1], refImg.shape[0]))
        
        
        s = ssim(refImg,curImg)
        print "SSIM with null picture:\t"+str(s)
        if s>0.99:
            return True
    return False
    

def sanitize_text(s):
    s = unicode(s)
    s = s.replace("&#39;"," ")
    s = s.replace("'"," ")
    s = s.replace("�"," ")
    s = s.replace("\""," ")
    s = s.replace("\n"," ")
    s = ftfy.fix_text(s)
    return s
        
def photo_crawling(photo_id,):
    
    try:
   
    check = False
        print "\nProcessing photo with FlickrId:\t" +photo_id

        print "Getting photo info..."
        response = flickr.photos.getInfo(api_key = api_key, photo_id=photo_id)            
        print "request result:\t"+response['stat']
        photo_info = response['photo']
        ext = 'jpg'
        photo_url = 'https://farm'+str(photo_info['farm'])+'.staticflickr.com/'+str(photo_info['server'])+'/'+photo_id+'_'+str(photo_info['secret'])+'_b.'+ext
        user_id = photo_info['owner']['nsid']

        dt = datetime.datetime.utcnow()
        date_download = calendar.timegm(dt.utctimetuple())
             
    date_posted = photo_info['dates']['posted']
    date_taken = photo_info['dates']['posted']
    photo_title = ''
    photo_title = photo_info['title']['_content']
    photo_title = sanitize_text(photo_title)

    photo_description = ''
    photo_description = photo_info['description']['_content']
    photo_description = sanitize_text(photo_description)

    photo_tags_num = len(photo_info['tags']['tag'])
    photo_tags = [sanitize_text(str(t['_content'])) for t in photo_info['tags']['tag']]

    lat = ""
    lon = ""
    country = ""
    if 'location' in photo_info:
        lat = photo_info['location']['latitude']
        lon = photo_info['location']['longitude']
        if 'country' in photo_info['location']:
            country = photo_info['location']['country']['_content']


    print "Getting photo sizes..."
    response = flickr.photos.getSizes(api_key = api_key, photo_id=photo_id)            
    print "request result:\t"+response['stat']
    photo_size = 0
    for sz in response['sizes']['size']:
        if sz['label'] == 'Original':
            print "Original size:\t("+ sz['height'] +" X " + sz['width'] +")"
            break

    # Download the photo and check if still available
    img_path = "images/"+photo_id   
    httpRes = urllib.urlretrieve(photo_url, img_path)
    abs_path = os.path.abspath(img_path)
        
    check = isMissing(img_path)
    print "Is missing:\t" + str(check)
    if check:
        copyfile(abs_path, "missing/"+photo_id)
        os.remove(abs_path)
        abs_path = os.path.abspath("missing/"+photo_id)            
    elif os.stat(abs_path).st_size < 10000:
    # Discard images with size lower than 10Kb
        copyfile(abs_path, "missing/"+photo_id)
        os.remove(abs_path)
        abs_path = os.path.abspath("missing/"+photo_id)            
        check = True
    print "Photo discarded due to image file size"

    print "Getting photo contexts..." 
    response = flickr.photos.getAllContexts(api_key = api_key, photo_id=photo_id)            
    print "request result:\t"+response['stat']
    photo_sets = 0
    photo_groups = 0
    avg_group_memb =0
    avg_group_photos = 0
    photo_groups_ids =[]
    groups_members =[]
    groups_photos = []
    if 'set' in response:
        photo_sets = len(response['set'])
    if 'pool' in response:
        photo_groups = len(response['pool'])
        photo_groups_ids = [g['id'] for g in response['pool']]
        groups_members = [int(g['members']) for g in response['pool']]
        groups_photos = [int(g['pool_count']) for g in response['pool']]
        avg_group_memb = 0 if len(groups_members)==0 else np.mean(groups_members)
        avg_group_photos = 0 if len(groups_photos)==0 else np.mean(groups_photos)
    #    if photo_groups > 0:
    #    print json.dumps(photo_groups_ids)
    print "The photo is shared through\t" +str(photo_sets)+"\talbums and\t"+str(photo_groups)+"\tgroups."


    print "Getting user info..."
    response = flickr.people.getInfo(api_key = api_key, user_id=user_id)            
    print "request result:\t"+response['stat']
    ispro = int(response['person']['ispro'])
    has_stats = int(response['person']['has_stats']) 
    username = response['person']['username']['_content']
    username = sanitize_text(username)

    user_photos = int(response['person']['photos']['count']['_content'])        

     
    # Notes: the API allows only 500 photos per call. If the user has 
    # a huge amount of pictures, consider only the views of the 10.000 oldest photos (i.e., 20 pages).
    print "Getting photos stats..."
    user_photo_views = []
    page_n = 1
    while len(user_photo_views)< user_photos:
        if page_n == 10:
        print "Waiting 1 sec..."
        time.sleep(1)
        
        response = flickr.people.getPublicPhotos(api_key = api_key, user_id=user_id, extras='views', page=page_n, per_page=500)            
        print "request result:\t"+response['stat']
        page_elements = [int(p['views']) for p in response['photos']['photo']]
        user_photo_views.extend(page_elements)
        page_n += 1
        #Integrity check and upper bound                
        if len(page_elements)<500 or page_n >20:
        break

    user_mean_views = 0 if len(user_photo_views)==0 else np.mean(user_photo_views)
    print "The user's photos have a mean view rate of\t"+str(user_mean_views)+"\tcomputed on\t"+str(len(user_photo_views))+"\tphotos."

    print "Getting user groups info..."
    response = flickr.people.getPublicGroups(api_key = api_key, user_id=user_id)            
    print "request result:\t"+response['stat']
    user_groups_membs = [int(g['members']) for g in response['groups']['group']]
    user_groups_photos = [int(g['pool_count']) for g in response['groups']['group']]
    user_groups = len(user_groups_photos)
    avg_user_gmemb = 0 if len(user_groups_membs)==0 else np.mean(user_groups_membs)
    avg_user_gphotos = 0 if len(user_groups_photos)==0 else np.mean(user_groups_photos)

    response = flickr.contacts.getPublicList(api_key = api_key, user_id=user_id)
    contacts = int(response['contacts']['total'])
    print "The user has\t"+str(contacts)+"\tcontacts and is enrolled in\t" +str(user_groups)+"\tgroups with\t"+str(avg_user_gmemb)+"\tmean members and\t"+str(avg_user_gphotos)+"\tmean photos."



    # Single quote escape (serve se si deve usare il testo per una SQL INSERT)
    tags = json.dumps(photo_tags).replace("'","''")
    print tags
    
    
        photo_views = int(photo_info['views'])
        photo_comments = int(photo_info['comments']['_content'])        

        print "Getting photo favorites..."
        response = flickr.photos.getFavorites(api_key = api_key, photo_id=photo_id)            
        print "request result:\t"+response['stat']
        photo_favorites = int(response['photo']['total'])
     
        print photo_url,'\n','views:\t',photo_views,'\tcomments:\t',photo_comments

        return True    # The only one "True" return value
        
    except Exception, e:
        print "ERROR with Photo\t"+photo_id+":"
        print str(e)
        return False
    

def photos_analysis(images_list):

    err_list = []

    print "\n\nSTART\n************\tUTC\t"+str(datetime.datetime.utcnow())+"\t************"
    print "Analysing\t"+str(len(images_list))+"\t photos"

    for photo_id in images_list:
       try:
           res = photo_crawling(photo_id) 
           if res:
            print "Image\t"+photo_id+"\tanalyzed."
           else:
            print "Image\t"+photo_id+"\terror occurred."
            err_list.append(photo_id)
       except Exception, e:
        print "ERROR (external loop):"
        print "FlickrId:\t"+str(photo_id)
        err_list.append(photo_id)
        print str(e)
        
    print "Error images list:"
    print err_list
    print "Images with errors:"
    print len(err_list)

    print "\n\nEND\n************\tUTC\t"+str(datetime.datetime.utcnow())+"\t************"


# Null image name
nullImages= ["flickrMissing", "flickrNotFound"]

# Credenziali The Social Picture
api_key = u'INSERIRE LA PROPRIA API KEY'
api_secret = u'INSERIRE IL PROPRIO API SECRET'

# Le seguenti due righe sono state eseguite una volta sola per generare il token di autorizzazione    
#flickr = flickrapi.FlickrAPI(api_key, api_secret)
#flickr.authenticate_via_browser(perms='read')

flickr = flickrapi.FlickrAPI(api_key, api_secret, format='parsed-json')

photos_analysis([u'37474303502',u'36717094514',u'36783353383', u'23601757988', u'36783729883', u'36783687273'])



