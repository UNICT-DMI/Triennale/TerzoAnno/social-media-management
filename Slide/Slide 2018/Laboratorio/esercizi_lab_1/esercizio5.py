# Esercizio 5
# Definire una matrice 3x4, poi:
# Stampare la prima riga della matrice;
# Stampare la seconda colonna della matrice;
# Sommare la prima e l'ultima colonna della matrice;
# Sommare gli elementi lungo la diagonale principale;
# Stampare il numero di elementi della matrice.
import numpy as np

mat = np.array([[1,2,3,4],[5,6,7,8],[9,10,11,12]])
print (mat[0])
print (mat[:,1])

print (mat[:,0])
print (mat[:,-1])

print (sum(mat.diagonal()))

print (mat.shape[0]*mat.shape[1])
