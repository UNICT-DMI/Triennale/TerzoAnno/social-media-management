# Esercizio 6
# Si generi un array di 100 numeri tra 2 e 4 e si calcoli la somma degli 
# elementi i cui quadrati hanno un valore maggiore di 8

import numpy as np

x = np.linspace(2,4, 100)
print (sum(i for i in x if i**2>8))
