# Esercizio 1
#
#Definire la lista [1,8,2,6,15,21,76,22,0,111,23,12,24], dunque:
#
#Stampare il primo e l'ultimo numero della lista;
#Stampare la somma dei numeri con indici dispari (e.g., 1,3,5,...) nella lista;
#Stampare la lista ordinata in senso inverso;
#Stampare la media dei numeri contenuti nella lista.

list = [1,8,2,6,15,21,76,22,0,111,23,12,24]

# list[start:stop:step]

print (list[0], " ", list[-1])
print (sum(list[::2]))
print (list[::-1])
print (sum(list)/len(list))
