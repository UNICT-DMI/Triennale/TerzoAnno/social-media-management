# Esercizio 2
#
#Si definisca un dizionario mesi che mappi i nomi dei mesi nei loro corrispettivi numerici.
#Ad esempio, il risultato di:
#
# print mesi['Gennaio']
#
#deve essere
# 1

mesi = [
  "Gennaio",
  "Febbraio",
  "Marzo",
  "Aprile",
  "Maggio",
  "Giugno",
  "Luglio",
  "Agosto",
  "Settembre",
  "Ottobre",
  "Novembre",
  "Dicembre"
]

mesi_ = {}

for idx, val in enumerate(mesi):
  mesi_[val] = idx+1

print (mesi_["Gennaio"])




